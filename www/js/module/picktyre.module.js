
var picktyre = angular.module('picktyre.module', []);
picktyre
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app.pickveh', {// removed blank.login
        url: "/pickveh",
        views: {
            'menuContent': {// removed blankView
                templateUrl: "template/user/PickVehicle.html",
                controller: 'DashController'
            }
        }
    })
    .state('home.requestQuote', {// removed blank.login
        url: "/requestQuote",
        views: {
            'homeView': {// removed blankView
                templateUrl: "template/user/RequestQuote.html",
                controller: 'DashController'
            }
        }
    })
});
