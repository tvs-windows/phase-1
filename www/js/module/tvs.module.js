/*************************************************************************************
 * Application Name: TVS
 * Version: 2.0
 * Module Name: tvs.module
 * Description: Module is responsible for Following things
 * 1. Gathering all custom modules inside the app
 * 2. Gathering all angular-ionic modules inside the app
 * 3. Routing mechanium
 * 4. Global variables
 * 5. Constant variable
 * This is the main module with controlers everything about the app
 * Developer: Suchandra Banerjee
 **************************************************************************************/

var ngAppModule = angular.module('tvs.module', [
    'ionic',
    'ngCordova',
    'tips.module',
    'picktyre.module',
    'tips.module',
    'getintouchtab.module'
])


        .run(function ($ionicPlatform, $q, $log, $state, $rootScope) {

            $ionicPlatform.ready(function (device) {
                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
                //  $scope.pushdata={};
                var deferred = $q.defer();



            });



        })

        .config(function ($ionicConfigProvider, $stateProvider, $urlRouterProvider) {
            $ionicConfigProvider.backButton.previousTitleText(false).text('');

            // ============= template declaration ====================
            $stateProvider
                    .state('blank', {
                        url: "/blank",
                        abstract: true,
                        templateUrl: "template/abs/BlankTemplate.html"
                    })
                    .state('home', {
                        url: "/home",
                        abstract: true,
                        templateUrl: "template/abs/HomeTemplate.html"
                    })
                    .state('app', {
                        url: "/app",
                        cache: false,
                        abstract: true,
                        templateUrl: "template/abs/MenuTemplate.html"
                    })

                    .state('tab', {
                        url: "/tab",
                        abstract: true,
                        templateUrl: "template/abs/TabTemplate.html"
                    })

                    .state('getintab', {
                        url: "/getintab",
                        abstract: true,
                        templateUrl: "template/abs/GetInTouchTab.html"
                    })
            // ============= page declaration ====================

                    .state('blank.login', {// removed blank.login
                        url: "/login",
                        views: {
                            'blankView': {// removed blankView
                                templateUrl: "template/common/Login.html",
                                controller: 'LoginController'
                            }
                        }
                    })
                    .state('blank.register', {// removed blank.login
                        url: "/register",
                        views: {
                            'blankView': {// removed blankView
                                templateUrl: "template/common/registration.html",
                                controller: 'LoginController'
                            }
                        }
                    })
                    .state('app.dash', {// removed blank.login
                        url: "/dash",
                        views: {
                            'menuContent': {// removed blankView
                                templateUrl: "template/common/DashBoard.html",
                                controller: 'LoginController'
                            }
                        }
                    })
                    .state('home.fpwd', {// removed blank.login
                        url: "/fpwd",
                        views: {
                            'homeView': {// removed blankView
                                templateUrl: "template/common/ForgotPwd.html",
                                controller: 'LoginController'
                            }
                        }
                    })

            $urlRouterProvider.otherwise('/blank/login');
        });
