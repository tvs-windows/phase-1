
var tips = angular.module('tips.module', []);
tips
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('tab.tyrecare', {// removed blank.login
        url: "/tyrecare",
        views: {
            'tyrecare': {// removed blankView
                templateUrl: "template/user/tyrecare.html",
                controller: 'TipsController'
            }
        }
    })
    .state('tab.drivingtips', {// removed blank.login
        url: "/drivingtips",
        views: {
            'drivingtips': {// removed blankView
                templateUrl: "template/user/drivingtips.html",
                controller: 'TipsController'
            }
        }
    })
    .state('tab.weeklytips', {// removed blank.login
        url: "/weeklytips",
        views: {
            'weeklytips': {// removed blankView
                templateUrl: "template/user/weeklytips.html",
                controller: 'TipsController'
            }
        }
    })
    .state('home.tipsdetail', {// removed blank.login
        url: "/tipsdetail",
        views: {
            'homeView': {// removed blankView
                templateUrl: "template/user/TipsDetail.html",
                controller: 'TipsController'
            }
        }
    })


});
