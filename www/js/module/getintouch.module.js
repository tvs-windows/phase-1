
var getintouchtab = angular.module('getintouchtab.module', []);
getintouchtab
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('getintab.customerservices', {// removed blank.login
        url: "/customerservices",
        views: {
            'customerservices': {// removed blankView
                templateUrl: "template/user/customerservices.html",
                controller: 'GetInTouchController'
            }
        }
    })
    .state('getintab.sendfeedback', {// removed blank.login
        url: "/sendfeedback",
        views: {
            'sendfeedback': {// removed blankView
                templateUrl: "template/user/sendfeedback.html",
                controller: 'GetInTouchController'
            }
        }
    })
    .state('getintab.contactus', {// removed blank.login
        url: "/contactus",
        views: {
            'contactus': {// removed blankView
                templateUrl: "template/user/contactus.html",
                controller: 'GetInTouchController'
            }
        }
    })
    // .state('home.tipsdetail', {// removed blank.login
    //     url: "/tipsdetail",
    //     views: {
    //         'homeView': {// removed blankView
    //             templateUrl: "template/user/TipsDetail.html",
    //             controller: 'TipsController'
    //         }
    //     }
    // })


});
